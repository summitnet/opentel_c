package main

import (
    "log"
    "golang.org/x/net/icmp"
    "golang.org/x/net/ipv4"
    "os"
    "time"
//    "fmt"
    "net"
)

//need to implement timeout
func main(){

    c, err := icmp.ListenPacket("udp4", "0.0.0.0")
    if err != nil {
        log.Fatal("here",err)
    }
    defer c.Close()
    for{
         wm := icmp.Message{
             Type: ipv4.ICMPTypeEcho, Code: 0,
             Body: &icmp.Echo{
                 ID: os.Getpid() & 0xffff, Seq: 1,
                 Data: []byte("01213456789ABCDEF"),
             },
         }
         wb, err := wm.Marshal(nil)
        if err != nil {
            log.Fatal(err)
        }
        begin:=time.Now();
        if _, err := c.WriteTo(wb, &net.UDPAddr{IP: net.ParseIP("8.8.8.8"),}); err != nil {
            log.Fatal(err)
        }
        rb := make([]byte, 1500)
        n, peer, err := c.ReadFrom(rb)
        if err != nil {
            log.Fatal(err)
        }
        rm, err := icmp.ParseMessage(1, rb[:n])
        if err != nil {
            log.Fatal(err)
        }
        rtt:=time.Since(begin)
        switch rm.Type {
            case ipv4.ICMPTypeEchoReply:
                log.Printf("got reflection from %v time %v", peer,rtt)
            default:
                log.Printf("got %+v; want echo reply time %v", rm,rtt)
        }
        time.Sleep(15*time.Second)

    }

}