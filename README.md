# README #
**Links For better understanding**

**[sample code from opentelemtry collector modified for metric collection](https://github.com/open-telemetry/opentelemetry-go/blob/master/example/otel-collector/main.go) in otlp.go(Application/Edge side) and otel-local-config.yaml for opentelemtry collector configuration (to collect values from Application)**

### What is this repository for? ###

* Opentelemtry Collector to export metrics to prometheus 
* Experemental phase 

### More Links for Reference ###

* [Lighstep 101 outdated but good starting point](https://lightstep.com/blog/opentelemetry-101-what-are-metrics/)
* [maturity stage of opentelemetry collector](https://github.com/open-telemetry/opentelemetry-proto)
* [issue link](https://github.com/open-telemetry/opentelemetry-collector/issues/1255)
* [different metric collections gist version of it](https://github.com/open-telemetry/opentelemetry-specification/blob/master/specification/metrics/api.md#metric-instruments)
* [all metrics](https://pkg.go.dev/go.opentelemetry.io/otel@v0.10.0/api/metric?tab=doc)
* [useful collector configurations](https://github.com/open-telemetry/opentelemetry-go/tree/master/example/otel-collector)
* [example overall flow](https://www.youtube.com/watch?v=W3KcfZgVoq0)


### How do I get set up? ###

* Setup done in local environment read [opentlemetry docs](https://opentelemetry.io/docs/collector/about/) 
* Setup go [libraries](https://github.com/open-telemetry/opentelemetry-go) via go get may need for libraries.
