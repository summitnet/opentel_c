package main

import (
	"context"
	"fmt"
	"log"
	"time"
    "golang.org/x/net/icmp"
    "golang.org/x/net/ipv4"
    "net"
    "os"
	"google.golang.org/grpc"
	"os/exec"
	"go.opentelemetry.io/otel/api/global"
	"go.opentelemetry.io/otel/api/kv"
	"go.opentelemetry.io/otel/api/unit"
	"go.opentelemetry.io/otel/api/metric"
//	"go.opentelemetry.io/otel/api/standard"
	"go.opentelemetry.io/otel/exporters/otlp"
	"go.opentelemetry.io/otel/sdk/metric/controller/push"
	"go.opentelemetry.io/otel/sdk/metric/selector/simple"
//	"go.opentelemetry.io/otel/sdk/resource"
//	sdktrace "go.opentelemetry.io/otel/sdk/trace"
)

//in order to work with windows plz compile while replace the following lines 
/*

c, err := icmp.ListenPacket("udp4", "0.0.0.0") - > c, err := icmp.ListenPacket("ip4:icmp", "0.0.0.0")
c.WriteTo(wb, &net.UDPAddr{IP: net.ParseIP("8.8.8.8"),}) - > c.WriteTo(wb, &net.IPAddr{IP: net.ParseIP("8.8.8.8"),})



*/ 

func ping()float64{
    c, err := icmp.ListenPacket("udp4", "0.0.0.0")
    if err != nil {
        log.Fatal("here",err)
    }
    defer c.Close()
     wm := icmp.Message{
         Type: ipv4.ICMPTypeEcho, Code: 0,
         Body: &icmp.Echo{
             ID: os.Getpid() & 0xffff, Seq: 1,
             Data: []byte("01213456789ABCDEF"),
         },
     }
     wb, err := wm.Marshal(nil)
    if err != nil {
        log.Fatal(err)
    }
    begin:=time.Now();
    if _, err := c.WriteTo(wb, &net.UDPAddr{IP: net.ParseIP("8.8.8.8"),}); err != nil {
        log.Fatal(err)
    }
    rb := make([]byte, 1500)
    n, peer, err := c.ReadFrom(rb)
    if err != nil {
        log.Fatal(err)
    }
    rm, err := icmp.ParseMessage(1, rb[:n])
    if err != nil {
        log.Fatal(err)
    }
    rtt:=time.Since(begin)
    switch rm.Type {
        case ipv4.ICMPTypeEchoReply:
            log.Printf("got reflection from %v time %v", peer,rtt)
        default:
            log.Printf("got %+v; want echo reply time %v", rm,rtt)
    }
    return rtt.Seconds()*1000.00


}
func initProvider() (*otlp.Exporter, *push.Controller) {

	exp, err := otlp.NewExporter(
		otlp.WithInsecure(),
		otlp.WithAddress("122.248.226.210:30080"),
		otlp.WithGRPCDialOption(grpc.WithBlock()), // useful for testing
	)
	handleErr(err,"test")

	pusher := push.New(
		simple.NewWithExactDistribution(),
		exp,
		push.WithPeriod(2*time.Second),
	)

	global.SetMeterProvider(pusher.Provider())
	pusher.Start()

	return exp, pusher
}

func main() {
	log.Printf("Waiting for connection...")

	exp, pusher := initProvider()
	defer func() { handleErr(exp.Stop(), "failed to stop exporter") }()
	defer pusher.Stop() // pushes any last exports to the receiver

	meter := global.Meter("test-meter")

	// labels represent additional key-value descriptors that can be bound to a
	// metric observer or recorder.
	out,err:=exec.Command("hostname").Output();
	hostname:=string(out)
	if(err!=nil){
		fmt.Println(err)
	}
	commonLabels := kv.KeyValue(kv.String(hostname,"8.8.8.8"))


	// Recorder metric example
	valuerecorder,err := meter.NewFloat64UpDownCounter("latency",metric.WithUnit(unit.Milliseconds))
	handleErr(err,"lol")
	boundvalue:=valuerecorder.Bind(commonLabels)
	defer boundvalue.Unbind()
	fmt.Println(ping())
	ctx:=context.Background()
	for{
		boundvalue.Add(ctx,ping())
		time.Sleep(15*time.Second)
	}
}

func handleErr(err error, message string) {
	if err != nil {
		log.Fatalf("%s: %v", message, err)
	}
}